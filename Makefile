#
# Make all the tikz figures
#

all: bib aux
	pdflatex --shell-escape main.tex
	pdflatex --shell-escape supplement.tex
	pdflatex --shell-escape main.tex
	pdflatex --shell-escape supplement.tex
	pdflatex --shell-escape cover-letter

bib: aux
	bibtex main
	bibtex supplement

aux: main.tex supplement.tex
	pdflatex --shell-escape main.tex
	pdflatex --shell-escape supplement.tex

clean: 
	rm -rf *.aux *.csv *.log *.svg *.out *.nav *.toc *.rip *.vrb *.snm *.thm *~ *.bbl *.blg *.gz auto
