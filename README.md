# Smoothed Divergence Distributionally Robust Optimization

[LETTER](https://vanparys.gitlab.io/smooth-divergence-dro-siam/cover-letter.pdf)

[MAIN](https://vanparys.gitlab.io/smooth-divergence-dro-siam/main.pdf)

[SUPPLEMENT](https://vanparys.gitlab.io/smooth-divergence-dro-siam/supplement.pdf)

